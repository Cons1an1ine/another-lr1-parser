# О чем это? #

Очередной LR(1)-анализатор написанный "для себя" с целью понять базовые принципы построения компиляторов грамматик.
Так же закрепить знания о методах синтаксического анализа, в частности восходящего анализа. Никакой другой практической смысловой нагрузки данный "проект" не несёт. 

### Что внутри? ###

Солюшн созданный при помощи свободной среды разработки SharpDevelop версии 5.1 с проектами написанными под .NET Framework 4.0.(должен свободно открываться из Visual Studio 2012 и старше).
В составе содержит 3 проекта:
- LR Parser (dll-ка в которой происходит основное действие по анализу грамматик и текстов);
- TestProject (набор тестов для проекта LR Parser написанных при помощи NUnit версии 3.6.1);
- WPF Form (простая форма, для наглядной работы dll-ки);

### Как это работает? ###

LR Parser предоставляет два метода:
* void setGrammar(...)
* bool isMatch(...)

Вначале необходимо задать грамматику через метод setGrammar(...). Грамматика должна соответствовать следующим критериям:
1. Соответствовать множеству LR(1) грамматик.
2. Правила записываются в форме <порождающий нетерминал> ::= <тело продукции>, тело продукции может содержать как терминалы так и нетерминалы.
3. Порождающий нетерминал первого правила является стартовым символом всей грамматики.
4. Допускается использование регулярных выражений для определения терминалов, но только когда <тело продукции> состоит из данного терминала.
5. Грамматические символы в теле продукции отделяются друг от друга пробельными символами.
6. В строке допускается определение не больше одного правила.

Анализатор не умеет разрешать случаи неоднозначных грамматик.
После того, как грамматика была считана, для нее строится детерминированный конечный автомат, по которому происходит формирование таблиц ACTION и GOTO.
Данные таблицы будут использоваться для проверки входящего текста в методе isMatch на соответствие считанной грамматике.

После того, как таблицы синтаксического анализа были построенны, мы можем проверять тексты на соответствие данной грамматике при помощи метода isMatch(...).
Метод возвращает ожидаемые true/false значения.

Методы могут кидать исключения, они определены в проекте LR Parser и документированы в каких случаях они падают.

### Работа "из коробки" ###

Стартовым проектом назначен "WPF Form", он оборачивает формой работу с dllкой из проекта "LR Parser".
На форме находятся две области для ввода текста: для грамматики и проверяемого ей на соответствие текста.
А так же две кнопки, запускающие считывание грамматики/формирование таблиц анализа и проверку текста на соответствие грамматике.
Над текстовыми областями находится набор кнопок, нажав на которые мы заполним данные области готовыми примерами. 

### Чем пользовался автор? ###

* Все данные он получил из книги "Компиляторы. Принципы, технологии, инстументы" - за авторством Ахо, Лам, Сети и Ульмана, 2-ое издание.
* А так же огромнейшую пользу мне принес сайт [The Context Free Grammar Checker](http://smlweb.cpsc.ucalgary.ca/) за возможность брать там готовые примеры грамматик, а так же строить для них ДКА и таблицы синтаксического анализа.

### Что будет дальше? ###

Пока что ничего, возможно будет реализован механизм разрешения неоднозначных грамматик.
А так же генерации таблиц для LALR(1), LR(2)-анализаторов.
Или реализована работа с семантикой, и вместо простого анализатора, мы получим "еще один компилятор компиляторов".
Или ...