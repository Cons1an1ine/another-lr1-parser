﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 04/29/2017
 * Time: 23:18
 * 
 *
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace WPF_Form
{
    /// <summary>
    /// Description of Examples.
    /// </summary>
    public class Examples
    {
        private List<string> grammarsSamples;
        private List<string[]> textForParsing;
        
        public Examples(string path)
        {
            grammarsSamples = new List<string>();
            textForParsing = new List<string[]>();
            
            var builder = new StringBuilder();
            var examples = new List<string>();
            bool readingGrammar = true;
            
            using(var stream = new StreamReader(path))
            {
                while (!stream.EndOfStream) {
                    var str = stream.ReadLine();
                    if (string.IsNullOrWhiteSpace(str)) {
                        continue;
                    }
                    
                    if (str.Equals("[grammar]")) {
                        if (builder.Length != 0) {
                            if (readingGrammar) {
                                grammarsSamples.Add(builder.ToString());
                                textForParsing.Add(examples.ToArray());
                            } else {
                                examples.Add(builder.ToString());
                                textForParsing.Add(examples.ToArray());
                            }
                        }
                        builder.Clear();
                        examples.Clear();
                        readingGrammar = true;
                        continue;
                    }
                    
                    if (str.Equals("[example]")) {
                        if (builder.Length != 0) {
                            if (readingGrammar) {
                                grammarsSamples.Add(builder.ToString());
                            } else {
                                examples.Add(builder.ToString());
                            }
                        }
                        builder.Clear();
                        readingGrammar = false;
                        continue;
                    }
                    builder.AppendLine(str);
                }
            }
            
            if (builder.Length != 0) {
                if (readingGrammar) {
                    grammarsSamples.Add(builder.ToString());
                    textForParsing.Add(examples.ToArray());
                } else {
                    examples.Add(builder.ToString());
                    textForParsing.Add(examples.ToArray());
                }
            }
        }
        
        public List<string> GrammarsCollection
        {
            get { return this.grammarsSamples; }
        }
        
        public List<string[]> ExamplesCollection
        {
            get { return this.textForParsing; }
        }
    }
}
