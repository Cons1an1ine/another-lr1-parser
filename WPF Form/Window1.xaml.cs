﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 25.04.2017
 * Time: 21:39
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using LR_Parser;

namespace WPF_Form
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private Examples examples;
        private LRParser parser;
            
        public Window1()
        {
            InitializeComponent();
            InitializeExamples();
            this.parser = new LRParser();
        }
        
        private void InitializeExamples()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "\\Examples.txt";
            this.examples = new Examples(path);

            for(int i = 0; i <examples.GrammarsCollection.Count; ++i) {
                var grammarBtn = new Button();
                grammarBtn.MinHeight = 25;
                grammarBtn.MinWidth = 25;
                grammarBtn.Content = i.ToString();
                grammarBtn.Margin = new Thickness(5, 5, 0, 0);
                
                var examplesBtns = new List<Button>(examples.ExamplesCollection[i].Length);
                for(int j = 0; j < examples.ExamplesCollection[i].Length; ++j) {
                    var examplBtn = new Button();
                    examplBtn.MinHeight = 25;
                    examplBtn.MinWidth = 25;
                    examplBtn.Content = j.ToString();
                    examplBtn.Margin = new Thickness(5, 5, 0, 0);
                    
                    var text = examples.ExamplesCollection[i][j];
                    examplBtn.Click += (o, e) => MatchingText.Text = text;
                    examplesBtns.Add(examplBtn);
                }
                    
                var grammartxt = examples.GrammarsCollection[i];
                grammarBtn.Click += (o, e) => { 
                    GrammarDefination.Text = grammartxt;
                    TextExamples.Children.Clear();
                    foreach(var btn in examplesBtns) {
                        TextExamples.Children.Add(btn);
                    }
                };
                Grammars.Children.Add(grammarBtn);
            }
        }
        void ReadGrammar_Click(object sender, RoutedEventArgs e)
        {
            try {
                parser.setGrammar(GrammarDefination.Text);
            } catch(RuleFormatException ex) {
        		MessageBox.Show("Формат правила не соблюдается. Номер правила: " + ex.RuleIndex);
        		GrammarDefination.Focus();
        		var startPosition = GrammarDefination.Text.IndexOf(ex.ParsingString);
        		GrammarDefination.Select(startPosition, ex.ParsingString.Length);
            } catch(NoRulesFoundException ex) {
                MessageBox.Show("Поле с грамматикой пусто. Напишите собственную или воспользуйтесь заготовками над областью ввода.");
            } catch(ActionConflictException ex) {
        		MessageBox.Show("Указанная грамматика не входит в множество LR1-грамматик" + 
        		               '\n' + "При построении LR-автомата в состоянии: " + ex.StateIndex + " произошел конфликт пунктов" +
        		              '\n' + ex.FirstItem + '\n' + ex.SecondItem);
            }
        }
        void MatchText_Click(object sender, RoutedEventArgs e)
        {
        	Color borderColor = Colors.Pink;
            try {
        		if (parser.isMatch(MatchingText.Text)) {
        			borderColor = Colors.LightGreen;
        		}
            } catch(TokenParsingException ex) {
        		var strPosition = ex.ErrorStringPosition;
        		MatchingText.Focus();
        		MatchingText.Select(strPosition, 5);
        		System.Windows.MessageBox.Show("Неудалось распознать лексему в проверяемом тексте.");
        	} catch(NoRulesFoundException ex) {
        		System.Windows.MessageBox.Show("Перед проверкой текста необходимо задать грамматику.");
        		return;
        	}
        	
        	var colorAnimation = new ColorAnimation();
        	colorAnimation.From = borderColor;
        	colorAnimation.Duration = TimeSpan.FromSeconds(2.5);
        	MatchingText.Background = new SolidColorBrush();
        	MatchingText.Background.BeginAnimation(SolidColorBrush.ColorProperty, colorAnimation);
        }
    }
}