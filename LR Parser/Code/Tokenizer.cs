﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 05.04.2017
 * Time: 18:55
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace LR_Parser.Code
{
    /// <summary>
    /// Класс осуществляющий поиск лексем (токенов) во входном потоке
    /// </summary>
    internal class Tokenizer
    {
        private List<int> symbolsIndexes;
        private List<Regex> regExpressions;
        private string parsingString;
        private int parsingStartPosition;
        private int currentSymbolID;
        
        /// <summary>
        /// Лексический анализатор
        /// </summary>
        /// <param name="symbols">Список грамматических символов</param>
        public Tokenizer(List<Symbol> symbols)
        {
            this.symbolsIndexes = new List<int>();
            this.regExpressions = new List<Regex>();
            
            Regex regExp = null;
            for(int i = 0; i < symbols.Count; ++i) {
                if (!symbols[i].isTerminal) {
                    continue;
                }
                
                try {
                    regExp = new Regex(@"\s*" + symbols[i].Text);
                } catch(ArgumentException) {
                    regExp = new Regex(@"\s*" + Regex.Escape(symbols[i].Text));
                }
                this.regExpressions.Add(regExp);
                this.symbolsIndexes.Add(i);
            }
            this.parsingString = string.Empty;
            setSource(this.parsingString);
        }
        
        /// <summary>
        /// Задать строку, которую будут разбивать на лексемы (токены)
        /// </summary>
        public void setSource(string text)
        {
            this.parsingString = text.Trim();
            this.currentSymbolID = Grammar.END_OF_STREAM;
            this.parsingStartPosition = 0;
            findNextToken();
        }
        
        /// <summary>
        /// Возвращает индекс текущего терминального символа или -2 (Grammar.END_OF_STREAM значение) в случае конца потока
        /// </summary>
        public int currentToken()
        {
            return this.currentSymbolID;
        }
        
        /// <summary>
        /// Найти следующий токен
        /// </summary>
        public void findNextToken()
        {
            if (this.parsingStartPosition >= this.parsingString.Length) {
                this.currentSymbolID = Grammar.END_OF_STREAM;
                return;
            }
            
            int bestMatchID = 0;
            int bestMatchLength = 0;
            
            // перебираем регулярные выражения соответствующие терминальным символам, самое длинное совпадение считаем искомым результатом
            for(int i = 0; i < this.regExpressions.Count; ++i) {
                var match = this.regExpressions[i].Match(this.parsingString, this.parsingStartPosition);
                if (match.Success && (match.Index == this.parsingStartPosition) && (match.Length > bestMatchLength)) {
                    bestMatchLength = match.Length;
                    bestMatchID = i;
                }
            }
            
            if (bestMatchLength != 0) {
                this.currentSymbolID = this.symbolsIndexes[bestMatchID];
                this.parsingStartPosition += bestMatchLength;
            } else {
                this.currentSymbolID = Grammar.END_OF_STREAM;
                throw new TokenParsingException(this.parsingStartPosition);
            }
        }
    }
}
