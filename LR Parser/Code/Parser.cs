﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 05.04.2017
 * Time: 18:44
 * 
 *
 */
using System;
using System.Collections.Generic;
using LR_Parser.Code.ActionStates;

namespace LR_Parser.Code
{
    /// <summary>
    /// Класс проверяющий последовательность лексем на соответствие заданной грамматике
    /// </summary>
    internal class Parser
    {
        private Grammar grammar;
        private List<Dictionary<int, IActionState>> actionTable;
        private List<Dictionary<int, int>> gotoTable;
        
        /// <param name="grammar">Грамматика языка</param>
        public Parser(Grammar grammar)
        {
            this.grammar = grammar;
            this.actionTable = null;
            this.gotoTable = null;
            
            createJumpTables();
        }
        
        private void createJumpTables()
        {
            var automat = new FSAutomat();
            automat.create(this.grammar, out this.actionTable, out this.gotoTable);
        }
        
        /// <summary>
        /// Проверить последовательность лексем на соответствие языку
        /// </summary>
        /// <param name="parsingStream">Лексический анализатор с проверяемой строкой</param>
        public bool match(Tokenizer parsingStream)
        {
            
            var states = new Stack<int>(); // стек состояний автомата
            var prefixes = new Stack<int>(); // стек хранящий активные префиксы
            states.Push(0);
            IActionState action = null;
            if(!this.actionTable[states.Peek()].TryGetValue(parsingStream.currentToken(), out action)) {
                return false;
            }
            //action = this.actionTable[states.Peek()][parsingStream.currentToken()];
            do {
                action.doAction(prefixes, states, parsingStream, this.gotoTable);
                if(!this.actionTable[states.Peek()].TryGetValue(parsingStream.currentToken(), out action)) {
                    return false;
                }
                //action = this.actionTable[states.Peek()][parsingStream.currentToken()];
            } while(action.GetType() != typeof(AcceptAction));
            if (prefixes.Count != 1) {
                return false;
            }
            return prefixes.Pop() == 0; // 0 - индекс стартового символа
        }
    }
}
