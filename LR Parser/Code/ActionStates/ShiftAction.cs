﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 27.02.2017
 * Time: 22:48
 * 
 *
 */
using System;
using LR_Parser.Code.ActionStates;
using System.Collections.Generic;

namespace LR_Parser.Code.ActionStates
{
    /// <summary>
    /// Действие Перенос (shift)
    /// </summary>
    internal class ShiftAction :  IActionState
    {
        private int symbol;
        private int stateIndex;
        
        /// <summary>
        /// Перенос
        /// </summary>
        /// <param name="symbol">Индекс символа, по которому происходит перенос</param>
        /// <param name="stateIndex">Индекс нового состояния, в которое переходим</param>
        public ShiftAction(int symbol, int stateIndex)
        {
            this.symbol = symbol;
            this.stateIndex = stateIndex;
        }
        
        void IActionState.doAction(Stack<int> prefixes, Stack<int> stack, Tokenizer stream, List<Dictionary<int, int>> gotoTable)
        {
            stack.Push(this.stateIndex);
            prefixes.Push(this.symbol);
            stream.findNextToken();
        }
    }
}
