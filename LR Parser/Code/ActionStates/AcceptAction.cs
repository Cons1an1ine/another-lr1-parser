﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 27.02.2017
 * Time: 22:59
 * 
 *
 */
using System;
using System.Collections.Generic;

namespace LR_Parser.Code.ActionStates
{
    /// <summary>
    /// Состояние принятия строки
    /// </summary>
    internal class AcceptAction : IActionState
    {
        public AcceptAction()
        {
        }
		
        void IActionState.doAction(Stack<int> prefixes, Stack<int> stack, Tokenizer stream, List<Dictionary<int, int>> gotoTable)
        {
            
        }
    }
}
