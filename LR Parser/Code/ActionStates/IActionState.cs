﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 27.02.2017
 * Time: 13:17
 * 
 *
 */
using System;
using System.Collections.Generic;

namespace LR_Parser.Code.ActionStates
{
    /// <summary>
    /// Интерфейс поведения элементов Action таблицы
    /// </summary>
    internal interface IActionState
    {
        /// <summary>
        /// Произвести действие с ДК-автоматом
        /// </summary>
        /// <param name="prefixes">Стек хранящий активный префикс(viable prefix)</param>
        /// <param name="stack">Стех хранящий состояния автомата</param>
        /// <param name="stream">Лексический анализатор с "проверяемой" на соответствие грамматике строкой</param>
        /// <param name="gotoTable">Таблица GOTO переходов(пары нетерминал: индекс нового состояния автомата)</param>
        void doAction(Stack<int> prefixes, Stack<int> stack, Tokenizer stream, List<Dictionary<int, int>> gotoTable);
    }
}
