﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 27.02.2017
 * Time: 22:59
 * 
 *
 */
using System;
using System.Collections.Generic;

namespace LR_Parser.Code.ActionStates
{
    /// <summary>
    /// Действие Свертка (reduce)
    /// </summary>
    internal class ReduceAction : IActionState
    {
        private int baseSymbol;
        private int productionLength;
        
        /// <summary>
        /// Свертка
        /// </summary>
        /// <param name="baseSymbols">Базовый символ</param>
        /// <param name="productionLength">Количество символов снимаемых со стека(длина тела продукции которую "сворачиваем")</param>
        public ReduceAction(int baseSymbols, int productionLength)
        {
            this.baseSymbol = baseSymbols;
            this.productionLength = productionLength;
        }
        
        void IActionState.doAction(Stack<int> prefixes, Stack<int> stack, Tokenizer stream, List<Dictionary<int, int>> gotoTable)
        {
            for(int i = 0; i < this.productionLength; ++i) {
                stack.Pop();
                prefixes.Pop();
            }
            stack.Push(gotoTable[stack.Peek()][this.baseSymbol]);
            prefixes.Push(this.baseSymbol);
        }
    }
}
