﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 03.01.2017
 * Time: 21:08
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LR_Parser.Code
{
    /// <summary>
    /// Класс отвечающий за представление грамматики
    /// </summary>
    internal class Grammar: IEquatable<Grammar>
    {
        private List<Symbol> symbols;
        private List<Rule> rules;
        private List<int>[] first;
        private List<int>[] follow;
        private bool[] nullable;
        private bool[] endable;
        public const int END_OF_STREAM = -2;

        public Grammar(List<Symbol> symbols, List<Rule> rules)
        {
            this.symbols = symbols;
            this.rules = rules;
            
            // Заполняет this.first и this.nullable
            findFirstAndNullableSets();
            // Заполняет this.follow и this.enable
            findFollowSet();
        }
        
        /// <summary>
        /// Список всех грамматических символов, принадлежащих грамматике
        /// </summary>
        public List<Symbol> Symbols {
            get { return this.symbols; }
        }
        
        /// <summary>
        /// Список всех правил грамматики
        /// </summary>
        public List<Rule> Rules {
            get { return this.rules; }
        }
        
        /// <summary>
        /// Список множеств First для каждого грамматического символа
        /// </summary>
        public List<int>[] First
        {
            get { return this.first; }
        }

        /// <summary>
        /// Маркеры присутствия в множествах First пустых продукций
        /// </summary>
        public bool[] Nullable
        {
            get { return this.nullable; }
        }

        /// <summary>
        /// Список множеств Follow для каждого нетерминального символа
        /// </summary>
        public List<int>[] Follow
        {
            get { return this.follow; }
        }
        
        /// <summary>
        /// Маркеры присутствия в множествах Follow $ символов(конец строки)
        /// </summary>
        public bool[] Endable
        {
            get { return this.endable; }
        }
        
        /// <summary>
        /// Возвращает правило грамматики
        /// </summary>
        /// <param name="baseSymbolIndex">Индекс порождающего символа</param>
        /// <returns>ссылку на правило или null в случае, когда правил с таким грамматическим символом не найдено</returns>
        public Rule getRule(int baseSymbolIndex)
        {
            var index = this.rules.FindIndex(x => x.BaseSymbol == baseSymbolIndex);
            if (index == -1)
                return null;
            return this.rules[index];
        }
        
        /// <summary>
        /// Возвращает индекс правила в списке правил грамматики
        /// </summary>
        /// <param name="baseSymbolIndex">Индекс порождающего символа</param>
        /// <returns>индекс правила, или -1, если с таким порождающим символом правило не найдено</returns>
        public int getRuleIndex(int baseSymbolIndex)
        {
            return this.rules.FindIndex(x => x.BaseSymbol == baseSymbolIndex);
        }

        private void findFirstAndNullableSets()
        {
            this.first = new List<int>[this.symbols.Count];
            this.nullable = new bool[this.symbols.Count];

            // Обходим все терминалы
            for (int i = 0; i < this.symbols.Count; ++i) {
                if (this.symbols[i].isTerminal) {
                    this.first[i] = new List<int> { i };
                    this.nullable[i] = false;
                }
            }

            // Определяем какие из множеств First нетерминалов будут содержать пустые грамматические символы (epsilon) 
            foreach (var rule in this.Rules) {
                this.nullable[rule.BaseSymbol] = isNullable(rule);
            }

            // Обходим все нетерминалы
            foreach (var rule in this.Rules) {
                this.first[rule.BaseSymbol] = findFirstSet(rule);
            }
        }

        /// <summary>
        /// Проверяет присутствуют ли пустые порождения в множестве базового нетерминала из входящего правила
        /// </summary>
        private bool isNullable(Rule rule)
        {
            if (rule.HaveEmptyProduction)
                return true;

            // список обработанных нетерминалов
            var inProcess = new Stack<int>();
            inProcess.Push(rule.BaseSymbol);

            var productions = new List<Production>();
            productions.AddRange(rule.Productions);

            for (int i = 0; i < productions.Count; ++i) {

                var symbolIndex = productions[i].Symbols.First();
                if (this.symbols[symbolIndex].isTerminal) {
                    continue; // Если терминал, то переходим к следующей продукции
                }

                // Достаем для нетерминала правило
                var symbolRule = getRule(symbolIndex);
                if (symbolRule.HaveEmptyProduction) {
                    return true; // Пустое порождение найдено
                }
                if (!inProcess.Contains(symbolIndex)) {
                    // дополняем список просматриваемых продукций
                    productions.AddRange(symbolRule.Productions);
                    inProcess.Push(symbolIndex);
                }
            }
            return false;
        }

        private List<int> findFirstSet(Rule rule)
        {
            // Достали из правила порождающий нетерминал
            var nonTerminal = rule.BaseSymbol;
            var result = new HashSet<int>();

            // Стек в который мы будем складывать уже "обработанные" нетерминалы
            var inProcessing = new Stack<int>(this.rules.Count);
            inProcessing.Push(nonTerminal);

            // Достаем продукции для данного нетерминала
            var productions = new List<Production>();
            productions.AddRange(rule.Productions);

            // Перебираем продукции из текущего правила
            for (int i = 0; i < productions.Count; ++i) {
                var productionSymbols = productions[i].Symbols;
                // Перебираем символы в продукции
                for (int j = 0; j < productionSymbols.Length; ++j) {
                    var currentSymbol = productionSymbols[j];
                    if (this.symbols[currentSymbol].isTerminal) {
                        // Если терминал, то сразу добавляем его в множество First
                        result.Add(currentSymbol);
                        break;
                    } else { // Нетерминал
                        var ruleByCurrentSymbol = getRule(currentSymbol);

                        if (!inProcessing.Contains(currentSymbol)) {
                            productions.AddRange(ruleByCurrentSymbol.Productions);
                            inProcessing.Push(currentSymbol);
                        }

                        // Если в его множестве First нет пустых продукций, то заканчиваем перебор данной продукции
                        if (!this.nullable[currentSymbol]) {
                            break;
                        }
                    }
                }
            }
            return result.ToList();
        }

        private void findFollowSet()
        {

            this.follow = new List<int>[this.rules.Count];
            this.endable = new bool[this.rules.Count];

            for (int i = 0; i < this.follow.Length; ++i) {
                this.follow[i] = new List<int>();
            }

            this.follow[0].Add(Grammar.END_OF_STREAM);
            this.endable[0] = true;

            // Массив пар индексов правил (нетерминалов) множество follow которых необходимо пересечь
            var followIntersect = new List<int[]>();

            // A ::= aBC, Follow(B) += First(C)
            // Перебираем правила
            for (int ruleIndex = 0; ruleIndex < this.rules.Count; ++ruleIndex) {
                // Перебираем продукции правила 
                foreach (var production in this.rules[ruleIndex].Productions) {
                    // Цикл по "левым" символам продукции
                    for (int leftProductionSymbol = 0; leftProductionSymbol < production.Symbols.Length; ++leftProductionSymbol) {
                        if (this.symbols[production.Symbols[leftProductionSymbol]].isTerminal) {
                            continue; // терминалы пропускаем
                        }

                        // Индекс правила, с соответствующим порождающим нетерминалом
                        var leftNonTerminal = getRuleIndex(production.Symbols[leftProductionSymbol]);

                        if (leftProductionSymbol + 1 == production.Symbols.Length) {
                            // Последний символ в данной продукции
                            followIntersect.Add(new int[2] { leftNonTerminal, ruleIndex });
                        }

                        // Цикл по "правым" символам продукции
                        // работает когда в продукции больше 2 символов
                        for (int rightProductionSymbol = leftProductionSymbol + 1; rightProductionSymbol < production.Symbols.Length; ++rightProductionSymbol) {
                            // Пополняем множество Follow с проверкой на совпадения
                            foreach (var symbolIndex in this.first[production.Symbols[rightProductionSymbol]]) {
                                if (!this.follow[leftNonTerminal].Contains(symbolIndex)) {
                                    this.follow[leftNonTerminal].Add(symbolIndex);
                                }
                            }
                            if (!this.nullable[production.Symbols[rightProductionSymbol]]) {
                                break;
                            }
                            if (rightProductionSymbol + 1 == production.Symbols.Length) {
                                followIntersect.Add(new int[2] { leftNonTerminal, ruleIndex });
                            }
                        }
                    }
                }
            }

            // A ::= aB(или aBw, где w содржит пустое порождение), Follow(B) += Follow(A)
            // пересекаем множества Follow исходя из заполненного выше followIntersect до тех пор, пока это возможно
            bool changed = true;
            while (changed) {
                changed = false;
                foreach (var pair in followIntersect) {
                    bool oldValue = this.nullable[pair[0]];
                    this.endable[pair[0]] = this.endable[pair[0]] || this.endable[pair[1]];
                    changed = oldValue != this.nullable[pair[0]];
                    foreach (var symbolIndex in this.follow[pair[1]]) {
                        if (!this.follow[pair[0]].Contains(symbolIndex)) {
                            this.follow[pair[0]].Add(symbolIndex);
                            changed = true;
                        }
                    }
                }
            }
        }

        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (object.ReferenceEquals(this, obj))
                return true;
            
            if (this.GetType() != obj.GetType())
                return false;
            
            return this.Equals(obj as Grammar);
        }
        
        public bool Equals(Grammar obj)
        {
            if (obj == null) {
                return false;
            }
            
            // first
            if (this.first.Length != obj.first.Length) {
                return false;
            }
            for(int i = 0; i < this.first.Length; ++i) {
                if (this.first[i].SequenceEqual(obj.first[i]) == false) {
                    return false;
                }
            }
            
            // follow
            if (this.follow.Length != obj.follow.Length) {
                return false;
            }
            for(int i = 0; i < this.follow.Length; ++i) {
                if (this.follow[i].SequenceEqual(obj.follow[i]) == false) {
                    return false;
                }
            }
            
            if (!this.nullable.SequenceEqual(obj.nullable)) {
                return false;
            }
            
            if (!this.endable.SequenceEqual(obj.endable)) {
                return false;
            }
            
            if ((this.symbols.Count != 0) || (obj.symbols.Count != 0)) {
                if (!this.symbols.SequenceEqual(obj.symbols)) {
                    return false;
                }
            }
            
            if ((this.rules.Count != 0) || (obj.rules.Count != 0)) {
                return this.rules.SequenceEqual(obj.rules);
            }
            
            return true;
        }
        
        public override string ToString()
        {
            var strBuilder = new StringBuilder(this.symbols.Count * 5);
            for (int i = 0; i < this.symbols.Count; ++i) {
                strBuilder.Append(i);
                strBuilder.Append(" - ");
                strBuilder.AppendLine(this.symbols[i].ToString());
            }
            
            strBuilder.AppendLine();
            foreach (var rule in this.rules) {
                strBuilder.AppendLine(rule.ToString());
            }
                        
            return strBuilder.ToString();
        }

    }
}
