﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 22.12.2016
 * Time: 17:20
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LR_Parser.Code
{
    /// <summary>
    /// Класс отвечающий за формирование грамматики из его текстового описания в форме Бэкуса-Наура
    /// </summary>
    internal class GrammarReader
    {
        private List<Symbol> symbols = null;
        private List<Rule> rules = null;
        private Regex inputMask;
        
        public GrammarReader()
        {
            string pattern = @"^\s*(?<baseSymbol>\w+)\s*\:\:\=\s*(?<production>(\S+\s*)+)?$";
            this.inputMask = new Regex(pattern, RegexOptions.IgnoreCase);
        }
        
        /// <summary>
        /// Создает грамматику описанную в тексовом потоке
        /// </summary>
        /// <param name="stream">Поток с грамматикой записанной в форме Бекуса-Наура</param>
        /// <returns>Сформированная грамматика</returns>
        public Grammar parse(StreamReader stream)
        {
            this.symbols = new List<Symbol>();
            this.rules = new List<Rule>();
            
            string baseSymbol = null;
            string[] production = null;
            var lineCount = 0;
            while (!stream.EndOfStream) {
                var str = stream.ReadLine();
                
                if (string.IsNullOrWhiteSpace(str)) {
                    continue;
                }
                ++lineCount;
                if (!this.inputMask.IsMatch(str)) {
                    throw new RuleFormatException(str, lineCount);
                }
                parseRule(str, out baseSymbol, out production);
                createRule(baseSymbol, production);
            }
            
            // Разметить все стартовые символы в правилах как нетерминальные
            foreach (var rule in this.rules) {
                this.symbols[rule.BaseSymbol].setNonterminalType();
            }
            if (this.rules.Count == 0) {
                throw new NoRulesFoundException();
            }
            return new Grammar(this.symbols, this.rules);
        }
        
        private void parseRule(string source, out string baseSymbol, out string[] production)
        {
            baseSymbol = this.inputMask.Match(source).Result("${baseSymbol}");
            
            var wholeProduction = this.inputMask.Match(source).Result("${production}").Trim();
            if (string.IsNullOrEmpty(wholeProduction)) {
                production = null;
                return;
            }
            
            char[] whitespace = { ' ', '\t' };
            production = wholeProduction.Split(whitespace, StringSplitOptions.RemoveEmptyEntries);            
        }
        
        private void createRule(string baseSymbol, string[] production)
        {
            // Добавляем новый грамматический символ, или получаем индекс, если он уже был добавлен
            int baseIndex = this.symbols.FindIndex(x => x.Text.Equals(baseSymbol));
            if (baseIndex == -1) {
                baseIndex = this.symbols.Count;
                this.symbols.Add(new Symbol(baseSymbol));
            }
            
            // Пробуем найти правило с таким базовым символом
            var ruleIndex = this.rules.FindIndex(x => x.BaseSymbol == baseIndex);
            
            if (production == null) {
                if (ruleIndex == -1) {
                    this.rules.Add(new Rule(baseIndex));
                } else {
                    this.rules[ruleIndex].HaveEmptyProduction = true;
                }
                return;
            }
            
            // Создаем продукцию к данному правилу, попутно добавляя новые грамматические символы, если они есть
            // В продукции хранятся не сами грамматические символы, а индексы на них
            List<int> prod = new List<int>(production.Length);
            foreach (var symbol in production) {
                int index = this.symbols.FindIndex(x => x.Text.Equals(symbol));
                if (index == -1) {
                    index = this.symbols.Count;
                    this.symbols.Add(new Symbol(symbol));
                }
                prod.Add(index);
            }
            
            // Создаем новое правило, или добавляем продукцию к существующему
            if (ruleIndex == -1) {
                this.rules.Add(new Rule(baseIndex, new Production(prod.ToArray())));
            } else {
                this.rules[ruleIndex].addProduction(new Production(prod.ToArray()));
            }
        }
    }
}
