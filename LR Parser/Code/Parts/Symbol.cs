﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 22.12.2016
 * Time: 13:45
 * 
 *
 */
using System;

namespace LR_Parser.Code
{
    /// <summary>
    /// Грамматический символ
    /// </summary>
    internal class Symbol: IEquatable<Symbol>
    {
        private string value;
        private bool terminal;
        
        /// <param name="symbol">Строка с символом</param>
        /// <param name="terminal">Значение терминальный он, или нет</param>
        public Symbol(string symbol, bool terminal)
        {
            this.value = symbol;
            this.terminal = terminal;
        }
        
        /// <param name="symbol">Строка с символом</param>
        public Symbol(string symbol)
            : this(symbol, true)
        {
        }
        
        /// <summary>
        /// Текстовое значение, которому соответствует данный грамматический символ.
        /// </summary>
        public string Text {
            get { return this.value; }
        }
        
        /// <summary>
        /// Флаг терминальности/нетерминальности символа
        /// </summary>
        public bool isTerminal {
            get { return this.terminal; }
            set { this.terminal = value; }
        }
        
        /// <summary>
        /// Изменить тип символа на нетерминальный
        /// </summary>
        public void setNonterminalType()
        {
            this.terminal = false;
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (object.ReferenceEquals(this, obj))
                return true;
            
            if (this.GetType() != obj.GetType())
                return false;
            
            return this.Equals(obj as Symbol);
        }
        
        public bool Equals(Symbol obj)
        {
            if (obj == null)
                return false;
            
            if (this.terminal != obj.terminal)
                return false;
            
            return this.value.Equals(obj.value);
        }
        
        public override string ToString()
        {
            return string.Format("[{0}, {1}]", this.value, this.terminal ? "Terminal" : "Nonterminal");
        }
    }
}
