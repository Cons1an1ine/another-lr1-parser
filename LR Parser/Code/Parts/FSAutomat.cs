﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 30.03.2017
 * Time: 13:03
 * 
 *
 */
using System;
using System.Collections.Generic;
using LR_Parser.Code.ActionStates;
using LR_Parser.Code.Parts;

namespace LR_Parser.Code
{
    /// <summary>
    /// Класс отвечающий за построение ДКА представляющего грамматику
    /// </summary>
    internal class FSAutomat
    {
        public FSAutomat()
        {
        }
        
        /// <summary>
        /// Построить ДКА и заполнить данными таблицы action и goto
        /// </summary>
        /// <param name="grammar">Грамматика языка</param>
        /// <param name="actionTable">Таблица действий reduce/shift/accept</param>
        /// <param name="gotoTable">Таблица переходов по нетерминалам</param>
        public void create(Grammar grammar, out List<Dictionary<int, IActionState>> actionTable, out List<Dictionary<int, int>> gotoTable)
        {
            actionTable = new List<Dictionary<int, IActionState>>();
            gotoTable = new List<Dictionary<int, int>>();
            
            var states = new List<AutomatState>();
            states.Add(createStartState(grammar));
            for(int stateIndex = 0; stateIndex < states.Count; ++stateIndex) {
                var actionPair = new Dictionary<int, IActionState>();
                var gotoPair = new Dictionary<int, int>();
                // Завершенные продукции
                foreach(var finished in states[stateIndex].FinishedItems) {
                    if (actionPair.ContainsKey(finished.Lookahead)) {
                		// конфликт свертка/свертка
                        var added = states[stateIndex].FinishedItems.FindIndex(x => x.Lookahead == finished.Lookahead);
                        throw new ActionConflictException(states[stateIndex].FinishedItems[added].ToString(),
                                                          finished.ToString(),
                                                          stateIndex);
                    }
                    actionPair.Add(finished.Lookahead, new ReduceAction(finished.getBaseSymbol(), finished.getDotValue()));
                }
                // "точка" стоит перед терминальным символом
                foreach(var terminal in states[stateIndex].Terminals) {
                    if (actionPair.ContainsKey(terminal)) {
                		// конфликт перенос/свертка или перенос/перенос
                        var added = states[stateIndex].FinishedItems.FindIndex(x => x.Lookahead == terminal);
                        var duplicate = states[stateIndex].Items.FindIndex(x => x.getDotSymbolIndex() == terminal);
                        throw new ActionConflictException(states[stateIndex].FinishedItems[added].ToString(),
                                                          states[stateIndex].Items[duplicate].ToString(),
                                                          stateIndex);
                        
                    }
                    actionPair.Add(terminal, findShift(states, stateIndex, terminal));
                }
                // "точка" стоит перед нетерминальным символом
                foreach(var nonTerminal in states[stateIndex].NonTerminals) {
                    gotoPair.Add(nonTerminal, findGOTO(states, stateIndex, nonTerminal));
                }
                
                actionTable.Add(actionPair);
                gotoTable.Add(gotoPair);
            }
            createEndState(grammar, states, actionTable, gotoTable);
        }

        private static AutomatState createStartState(Grammar grammar)
        {
            var items = new List<LRItem>();
            for (int production = 0; production < grammar.Rules[0].Productions.Count; ++production) {
                items.Add(new LRItem(grammar, 0, production, Grammar.END_OF_STREAM));
            }
            if (grammar.Rules[0].HaveEmptyProduction) {
                items.Add(new LRItem(grammar, 0, -1, Grammar.END_OF_STREAM));
            }
            return AutomatState.create(items, grammar);
        }

        private static void createEndState(Grammar grammar, List<AutomatState> states, List<Dictionary<int, IActionState>> actionTable, List<Dictionary<int, int>> gotoTable)
        {
            int startRule = 0;
            var startNonterminal = grammar.Rules[startRule].BaseSymbol;
            var finishState = states[0].makeTransition(startNonterminal);
            int finishIndex = states.IndexOf(finishState);
            if (finishIndex == -1) {
                states.Add(finishState);
                finishIndex = states.Count - 1;
                actionTable.Add(new Dictionary<int, IActionState>());
                gotoTable[startRule].Add(startNonterminal, states.Count - 1);
            }
            actionTable[finishIndex].Add(Grammar.END_OF_STREAM, new AcceptAction());
        }

        private static IActionState findShift(List<AutomatState> states, int stateIndex, int terminal)
        {
            var newState = states[stateIndex].makeTransition(terminal);
            if (!states.Contains(newState)) {
                states.Add(newState);
                return new ShiftAction(terminal, states.Count - 1);
            }
            return new ShiftAction(terminal, states.IndexOf(newState));
        }

        private static int findGOTO(List<AutomatState> states, int stateIndex, int nonTerminal)
        {
            var newState = states[stateIndex].makeTransition(nonTerminal);
            if (!states.Contains(newState)) {
                states.Add(newState);
                return states.Count - 1;
            }
            return states.IndexOf(newState);
        }
    }
}
