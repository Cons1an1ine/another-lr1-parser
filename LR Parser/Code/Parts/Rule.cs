﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 24.12.2016
 * Time: 21:58
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace LR_Parser.Code
{
    /// <summary>
    /// Правило грамматики
    /// </summary>
    internal class Rule: IEquatable<Rule>
    {
        private List<Production> productions;
        private int baseSymbol;
        private bool emptyProduction;
        
        /// <param name="baseSymbol">Индекс грамматического символа в списке всех символов грамматики</param>
        public Rule(int baseSymbol)
        {
            this.baseSymbol = baseSymbol;
            this.productions = new List<Production>();
            this.emptyProduction = true;
        }
        
        /// <param name="baseSymbol">Индекс грамматического символа в списке всех символов грамматики</param>
        /// <param name="production">Продукция правила</param>
        public Rule(int baseSymbol, Production production) : this(baseSymbol)
        {
            this.productions.Add(production);
            this.emptyProduction = false;
        }
        
        /// <param name="baseSymbol">Индекс грамматического символа в списке всех символов грамматики</param>
        /// <param name="productions">Список продукций для правила</param>
        public Rule(int baseSymbol, List<Production> productions) : this(baseSymbol)
        {
            this.productions.AddRange(productions);
            this.emptyProduction = false;
        }

        /// <summary>
        /// Индекс порождающего нетерминального символа данного правила
        /// </summary>
        public int BaseSymbol {
            get { return this.baseSymbol; }
        }
        
        /// <summary>
        /// Список продукций, содеражий индексы грамматических символов
        /// </summary>
        public List<Production> Productions {
            get { return this.productions; }
        }
        
        /// <summary>
        /// Есть ли пустая продукция из порождающего символа
        /// </summary>
        public bool HaveEmptyProduction
        {
            get { return this.emptyProduction; }
            set { this.emptyProduction = value; }
        }
        
        /// <summary>
        /// Добавление в правило еще одну продукцию
        /// </summary>
        /// <param name="production">Новая продукция</param>
        public void addProduction(Production production)
        {
            if (!this.productions.Contains(production))
                this.productions.Add(production);
        }
        
        public override string ToString()
        {
            return string.Format("Base {0} : Productions= {1}", this.baseSymbol, string.Join(", ", this.productions));
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (object.ReferenceEquals(this, obj))
                return true;
            
            if (this.GetType() != obj.GetType())
                return false;
            
            return this.Equals(obj as Rule);
        }
        
        public bool Equals(Rule obj)
        {
            if (obj == null)
                return false;
            
            if (this.baseSymbol != obj.baseSymbol)
                return false;
            
            if (!this.productions.SequenceEqual(obj.productions)) {
                return false;
            }
            
            return true;
        }
    }
}
