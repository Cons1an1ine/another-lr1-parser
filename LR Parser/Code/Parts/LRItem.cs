﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 28.03.2017
 * Time: 14:24
 * 
 *
 */
using System;
using System.Text;

namespace LR_Parser.Code.Parts
{
    /// <summary>
    /// Пункт грамматики (продукция с "точкой")
    /// </summary>
    internal class LRItem: IEquatable<LRItem>
    {
        private Grammar grammar;
        private int ruleIndex;
        private int productionIndex;
        /// <summary>
        /// Символ предпросмотра(его индекс)
        /// </summary>
        private int lookahead;
        /// <summary>
        /// Указывает индекс символа в продукции, перед которым стоит "точка".
        /// </summary>
        private int dotPosition;
        
        /// <summary>
        /// Грамматический пункт (Продукция с "точкой") 
        /// </summary>
        /// <param name="grammar">Ссылка на грамматику языка</param>
        /// <param name="ruleIndex">Номер правила в грамматике которому соответствует "пункт"</param>
        /// <param name="productionIndex">Номер индекса продукции. Если "пункт" состоит из пустой продукции то указать значение -1</param>
        /// <param name="lookahead"></param>
        public LRItem(Grammar grammar, int ruleIndex, int productionIndex, int lookahead)
        {
            this.grammar = grammar;
            this.ruleIndex = ruleIndex;
            this.productionIndex = productionIndex;
            this.dotPosition = 0;
            this.lookahead = lookahead;
        }
        
        public LRItem(LRItem item)
        {
            this.grammar = item.grammar;
            this.ruleIndex = item.ruleIndex;
            this.productionIndex = item.productionIndex;
            this.dotPosition = item.dotPosition;
            this.lookahead = item.lookahead;
        }
        
        /// <summary>
        /// Состоит ли пункт из единственной пустой продукции
        /// </summary>
        public bool IsEmpty
        {
            get { return this.productionIndex == -1; }
        }
        
        /// <summary>
        /// Символ предпросмотра(его индекс в грамматике)
        /// </summary>
        public int Lookahead
        {
            get {return this.lookahead;}
        }
        
        /// <summary>
        /// Продукция из правила на которой базируется данный пункт
        /// </summary>
        public Production Production
        {
            get { return this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex]; }
        }
        
        /// <summary>
        /// Сдвинуть "точку" в продукции
        /// </summary>
        /// <returns>Новый пункт или null, если точка уже стоит в самом конце продукции</returns>
        public LRItem next()
        {
            // .abc 0
            // a.bc 1
            // ab.c 2
            // abc. 3
            if (this.dotPosition >= this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols.Length) {
                return null;
            }
            var result = new LRItem(this);
            result.dotPosition += 1;
            return result;
        }
        
        /// <summary>
        /// Индекс порождающего нетерминала данного пункта
        /// </summary>
        public int getBaseSymbol()
        {
            return this.grammar.Rules[this.ruleIndex].BaseSymbol;
        }
        
        /// <summary>
        /// Возвращает грамматический символ, перед которым стоит "точка". null в случае конца продукции
        /// </summary>
        public Symbol getDotSymbol()
        {
            // .abc 0 return a
            // a.bc 1 return b
            // ab.c 2 return c
            // abc. 3 return null
            if (this.dotPosition >= this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols.Length) {
                return null;
            }
            var index = this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols[this.dotPosition];
            return this.grammar.Symbols[index];
        }
        
        /// <summary>
        /// Возвращает индекс терминала, перед которым стоит "точка". -1 в случае если "точка" стоит в конце продукции
        /// </summary>
        public int getDotSymbolIndex()
        {
            // .abc 0 return 0
            // a.bc 1 return 1
            // ab.c 2 return 2
            // abc. 3 return -1
            if (this.dotPosition >= this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols.Length) {
                return -1;
            }
            return this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols[this.dotPosition];
        }
        
        /// <summary>
        /// Возвращает позицию грамматического символа продукции, перед которым стоит "точка"
        /// </summary>
        public int getDotValue()
        {
            return this.dotPosition;
        }
        
        /// <summary>
        /// Возвращает номер правила, к которому относится данный пункт
        /// </summary>
        public int getRuleIndex()
        {
            return this.ruleIndex;
        }
        
        /// <summary>
        /// Возвращает индекс грамматического символа в грамматике
        /// </summary>
        /// <param name="indexInProduction">Индекс символа в продукции, -1 если найти не удалось</param>
        public int getSymbolIndex(int indexInProduction)
        {
            var productionSymbols = this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols;
            if (indexInProduction >= productionSymbols.Length) {
                return -1;
            }
            return productionSymbols[indexInProduction];
        }
        
        /// <summary>
        /// Возвращает грамматический символ из грамматики
        /// </summary>
        /// <param name="indexInProduction">Индекс символа в грамматике</param>
        /// <returns>Ссылка на объект или null в случае отсутствия</returns>
        public Symbol getSymbol(int indexInProduction)
        {
            var productionSymbols = this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols;
            if (indexInProduction >= productionSymbols.Length) {
                return null;
            }
            return this.grammar.Symbols[productionSymbols[indexInProduction]];
        }
        
        public override string ToString()
        {
            var builder = new StringBuilder();
            var baseIndex = this.grammar.Rules[this.ruleIndex].BaseSymbol;
            builder.Append(this.grammar.Symbols[baseIndex].Text).Append(" ::=");
            if (this.productionIndex == -1) {
                builder.Append(" .");
            } else {
                var production = this.grammar.Rules[this.ruleIndex].Productions[this.productionIndex].Symbols;
                for(int i = 0; i < production.Length; ++i) {
                    if (i == this.dotPosition) {
                        builder.Append(" .");
                    }
                    builder.Append(' ').Append(this.grammar.Symbols[production[i]].Text);
                }
                if (this.dotPosition == production.Length) {
                    builder.Append(" .");
                }
            }
            builder.Append(" ,");
            if (this.Lookahead == Grammar.END_OF_STREAM) {
                builder.Append(" \\$");
            } else {
                builder.Append(this.grammar.Symbols[this.lookahead].Text);
            }
            return builder.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (object.ReferenceEquals(this, obj))
                return true;
            
            if (this.GetType() != obj.GetType())
                return false;
            
            return this.Equals(obj as Rule);
        }
        
        public bool Equals(LRItem other)
        {
            if (other == null)
                return false;
            
            if (!this.grammar.Equals(other.grammar))
                return false;
            
            if (this.ruleIndex != other.ruleIndex)
                return false;
            
            if (this.productionIndex != other.productionIndex)
                return false;
            
            if (this.dotPosition != other.dotPosition)
                return false;
            
            if (this.lookahead != other.lookahead)
                return false;
            
            return true;
        }
    }
}
