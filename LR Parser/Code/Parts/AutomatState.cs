﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 28.03.2017
 * Time: 21:45
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LR_Parser.Code.Parts
{
    /// <summary>
    /// Состояние ДКА
    /// </summary>
    internal class AutomatState: IEquatable<AutomatState>
    {
        private HashSet<int> nonTerminals;
        private HashSet<int> terminals;
        private List<LRItem> items;
        private List<LRItem> finishedItems;
        private Grammar grammar;
        
        private AutomatState(List<LRItem> items, HashSet<int> nonTerminals, HashSet<int> terminals, List<LRItem> finishedItems, Grammar grammar)
        {
            this.items = items;
            this.nonTerminals = nonTerminals;
            this.terminals = terminals;
            this.finishedItems = finishedItems;
            this.grammar = grammar;
        }
        
        /// <summary>
        /// Нетерминальные символы, по которым можно осуществлять переход из данного состояния
        /// </summary>
        public HashSet<int> NonTerminals
        {
            get { return this.nonTerminals; }
        }
        
        /// <summary>
        /// Терминальные символы, по которым можно осуществлять переход из данного состояния
        /// </summary>
        public HashSet<int> Terminals
        {
            get { return this.terminals; }
        }
        
        /// <summary>
        /// Список с "законченными" пунктами предназначенный для определения сверток
        /// </summary>
        public List<LRItem> FinishedItems
        {
            get { return this.finishedItems; }
        }
        
        /// <summary>
        /// Список пунктов состояния, без "законченных".(Т.е. "точка" стоит не в конце )
        /// </summary>
        public List<LRItem> Items
        {
            get { return this.items; }
        }
        
        /// <summary>
        /// Строит состояние ДКА (замыкание множества пунктов)
        /// </summary>
        static public AutomatState create(List<LRItem> items, Grammar grammar)
        {
            var nonTerminals = new HashSet<int>();
            var terminals = new HashSet<int>();
            var finished = new List<LRItem>();
            var itemsCopy = new List<LRItem>(items.Count);
            
            foreach(var item in items) {
                itemsCopy.Add(new LRItem(item));
            }
            
            for(int itemIndex = 0; itemIndex < itemsCopy.Count; ++itemIndex) {
                // если пустой
                if (itemsCopy[itemIndex].IsEmpty) {
                    finished.Add(itemsCopy[itemIndex]);
                    continue;
                }
                
                var dotSymbolIndex = itemsCopy[itemIndex].getDotSymbolIndex();
                if (dotSymbolIndex == -1) {
                    // точка стоит в конце продукции
                    finished.Add(itemsCopy[itemIndex]);
                    continue;
                }
                
                if (itemsCopy[itemIndex].getDotSymbol().isTerminal) {
                    // точка стоит перед терминалом
                    terminals.Add(dotSymbolIndex);
                    continue;
                }
                
                // точка стоит перед нетерминалом
                nonTerminals.Add(dotSymbolIndex);
                // дополняем замыкание рассчитывая для каждого нового пункта символ предпросмотра
                // S ::= b.Aw, a
                var followDotValue = itemsCopy[itemIndex].getDotValue();
                var hasEmptyProduction = false;
                do {
                    ++followDotValue;
                    var followDotSymbol = itemsCopy[itemIndex].getSymbolIndex(followDotValue);
                    
                    // S ::= b.A , a
                    if (followDotSymbol == -1) { // followDotSymbol - за границей продукции
                        if (grammar.getRule(dotSymbolIndex).HaveEmptyProduction) {
                            // у этого нетерминала есть пустая продукция, dotSymbolIndex == A
                            // S ::= b.A , a
                            // A ::= , a
                            var emptyItem = new LRItem(grammar, grammar.getRuleIndex(dotSymbolIndex), -1, itemsCopy[itemIndex].Lookahead);
                            if (!itemsCopy.Contains(emptyItem)) {
                                itemsCopy.Add(emptyItem);
                            }
                        }
                        for(int productionIndex = 0; productionIndex < grammar.getRule(dotSymbolIndex).Productions.Count; ++productionIndex) {
                            // S ::= b.A , a
                            // A ::= .B, a
                            var newItem = new LRItem(grammar, grammar.getRuleIndex(dotSymbolIndex), productionIndex, itemsCopy[itemIndex].Lookahead);
                            if (!itemsCopy.Contains(newItem)) {
                                itemsCopy.Add(newItem);
                            }
                        }
                        break;
                    }
                    
                    // S ::= b.Aw, a
                    foreach(var symbolInFirst in grammar.First[followDotSymbol]) {
                        // S ::= b.Aw, a
                        // A ::= ., First(w)
                        if (grammar.getRule(dotSymbolIndex).HaveEmptyProduction) {
                            // у этого нетерминала есть пустая продукция
                            var emptyItem = new LRItem(grammar, grammar.getRuleIndex(dotSymbolIndex), -1, symbolInFirst);
                            if (!itemsCopy.Contains(emptyItem)) {
                                itemsCopy.Add(emptyItem);
                            }
                        }
                        // S ::= b.Aw, a
                        // A ::= .BC, First(w)
                        for(int productionIndex = 0; productionIndex < grammar.getRule(dotSymbolIndex).Productions.Count; ++productionIndex) {
                            var newItem = new LRItem(grammar, grammar.getRuleIndex(dotSymbolIndex), productionIndex, symbolInFirst);
                            if (!itemsCopy.Contains(newItem)) {
                                itemsCopy.Add(newItem);
                            }
                        }
                    }
                    if (grammar.Symbols[followDotSymbol].isTerminal) {
                        break;
                    }
                    // S ::= b.Aw, a
                    // w = cd, First(c) imp empty production
                    // повторяем вычисления, но уже с First(d)
                    hasEmptyProduction = grammar.getRule(followDotSymbol).HaveEmptyProduction;
                } while (hasEmptyProduction);
            }
            return new AutomatState(itemsCopy, nonTerminals, terminals, finished, grammar);
        }
                
        
        /// <summary>
        /// Сдвиг "точки" в пункте (прим.: A ::= .BC -> A ::= B.C)
        /// </summary>
        /// <param name="symbolIndex">Индекс грамматического символа, по которому будет сдвигаться "точка"</param>
        /// <returns> Список новых пунктов, которые получились после сдвига </returns>
        public AutomatState makeTransition(int symbolIndex)
        {
            var nextItems = new List<LRItem>();
            foreach(var item in this.items) {
                if (( ! item.IsEmpty ) && (item.getDotSymbolIndex() == symbolIndex)) {
                    var nextItem = item.next();
                    if (nextItem != null) {
                        nextItems.Add(nextItem);
                    }
                }
            }
            return create(nextItems, this.grammar);
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (object.ReferenceEquals(this, obj))
                return true;
            
            if (this.GetType() != obj.GetType())
                return false;
            
            return this.Equals(obj as AutomatState);
        }
        
        public bool Equals(AutomatState other)
        {
            if (other == null)
                return false;
            
            if (!this.terminals.SetEquals(other.terminals))
                return false;
            
            if (!this.nonTerminals.SetEquals(other.nonTerminals))
                return false;
            
            if (!this.grammar.Equals(other.grammar))
                return false;
            
            if (!this.finishedItems.SequenceEqual(other.finishedItems)){
                return false;
            }
            
            if (!this.items.SequenceEqual(other.items)){
                return false;
            }
            
            return true;
        }
        
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach(var item in this.items) {
                builder.AppendLine(item.ToString());
            }
            return builder.ToString();
        }

    }
}
