﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 24.12.2016
 * Time: 22:39
 * 
 *
 */
using System;
using System.Linq;

namespace LR_Parser.Code
{
    /// <summary>
    /// Продукция правила
    /// </summary>
    internal class Production: IEquatable<Production>
    {
        private int[] symbols;
        
        /// <param name="symbols">Список индексов грамматических символов продукции</param>
        public Production(int[] symbols)
        {
            this.symbols = symbols;
        }
        
        /// <summary>
        /// Индексы грамматических символов из которых состоит продукция
        /// </summary>
        public int[] Symbols {
            get { return this.symbols; }
        }
        
        public override string ToString()
        {
            return string.Format("[{0}]", string.Join(", ", this.symbols));
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (object.ReferenceEquals(this, obj))
                return true;
            
            if (this.GetType() != obj.GetType())
                return false;
            
            return this.Equals(obj as Production);
        }
        
        public bool Equals(Production obj)
        {
            if (obj == null)
                return false;
            
            if (!this.symbols.SequenceEqual(obj.symbols)) 
                return false;
            
            return true;
        }
    }
}
