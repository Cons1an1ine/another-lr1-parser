﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 20.04.2017
 * Time: 19:40
 * 
 *
 */
using System;
using System.IO;
using System.Text;
using LR_Parser.Code;

namespace LR_Parser
{
    /// <summary>
    /// LR1 анализатор
    /// </summary>
    public class LRParser
    {
        private Parser parser;
        private Tokenizer tokenizer;
        public LRParser()
        {
        	parser = null;
        	tokenizer = null;
        }
        
        /// <summary>
        /// Задать грамматику языка
        /// </summary>
        /// <param name="stream">Грамматика в соответствующей форме</param>
        /// <exception cref="RuleFormatException">Допущена ошибка в записи правила для грамматики</exception>
        /// <exception cref="NoRulesFoundException">Неудалось сформировать ни одного правила грамматики</exception>
        /// <exception cref="ActionConflictException">Ошибка при построении action таблицы LR1-автомата. Грамматика не принадлежит множеству LR1</exception>
        public void setGrammar(StreamReader stream)
        {
            var grReader = new GrammarReader();
            var grammar = grReader.parse(stream);
            
            tokenizer = new Tokenizer(grammar.Symbols);
            parser = new Parser(grammar);
        }
        
        /// <summary>
        /// Задать грамматику языка
        /// </summary>
        /// <param name="str">Грамматика в соответствующей форме</param>
        /// <exception cref="RuleFormatException">Допущена ошибка в записи правила для грамматики</exception>
        /// <exception cref="NoRulesFoundException">Неудалось сформировать ни одного правила грамматики</exception>
        /// <exception cref="ActionConflictException">Ошибка при построении action таблицы LR1-автомата. Грамматика не принадлежит множеству LR1</exception>
        public void setGrammar(string str)
        {
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                setGrammar(stream);
            }
        }
        
        /// <summary>
        /// Проверить текст на соответствие заданной до этого грамматике.
        /// </summary>
        /// <param name="text">Проверяемый текст</param>
        /// <returns>true - в случае соответствия, false во всех других случаях</returns>
        /// <exception cref="TokenParsingException">Лексический анализатор не смог выделить лексему из входного потока</exception>
        /// <exception cref="NoRulesFoundException">Грамматика не была задана</exception>
        public bool isMatch(string text)
        {
        	if ((parser == null) || (tokenizer == null)) {
        		throw new NoRulesFoundException();
        	}
            tokenizer.setSource(text);
            return parser.match(tokenizer);
        }
    }
}
