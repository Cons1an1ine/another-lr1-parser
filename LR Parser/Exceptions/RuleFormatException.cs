﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 21.04.2017
 * Time: 15:09
 * 
 *
 */
using System;
using System.Runtime.Serialization;

namespace LR_Parser
{
    /// <summary>
    /// Допущена ошибка в записи правила для грамматики
    /// </summary>
    public class RuleFormatException : Exception, ISerializable
    {
    	/// <summary>
    	/// Строка которую не удалось распарсить
    	/// </summary>
        public string ParsingString { get; set; }
        
        /// <summary>
        /// Номер правила, которое пытались распарсить
        /// </summary>
        public int RuleIndex { get; set; }
        
        public RuleFormatException(string parsingString, int ruleIndex)
        {
            this.ParsingString = parsingString;
            this.RuleIndex = ruleIndex;
        }
        
        public RuleFormatException()
        {
            init();
        }

         public RuleFormatException(string message) : base(message)
        {
             init();
        }

        public RuleFormatException(string message, Exception innerException) : base(message, innerException)
        {
            init();
        }
        
        private void init()
        {
            this.ParsingString = "";
            this.RuleIndex = -1;
        }

        // This constructor is needed for serialization.
        protected RuleFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info != null) {
                this.ParsingString = info.GetString("ParsingString");
                this.RuleIndex = info.GetInt32("RuleIndex");
            }
        }
        
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("ParsingString", this.ParsingString);
            info.AddValue("RuleIndex", this.RuleIndex);
        }
    }
}