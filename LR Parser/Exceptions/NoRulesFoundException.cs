﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 21.04.2017
 * Time: 15:10
 * 
 *
 */
using System;
using System.Runtime.Serialization;

namespace LR_Parser
{
    /// <summary>
    /// Неудалось сформировать ни одного правила грамматики
    /// </summary>
    public class NoRulesFoundException : Exception, ISerializable
    {
        public NoRulesFoundException()
        {
        }

         public NoRulesFoundException(string message) : base(message)
        {
        }

        public NoRulesFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected NoRulesFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}