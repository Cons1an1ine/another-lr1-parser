﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 21.04.2017
 * Time: 15:10
 * 
 *
 */
using System;
using System.Runtime.Serialization;

namespace LR_Parser
{
    /// <summary>
    /// Лексический анализатор не смог выделить лексему из входного потока
    /// </summary>
    public class TokenParsingException : Exception, ISerializable
    {
    	/// <summary>
    	/// Номер позиции во входном потоке, с которого пытались прочитать лексему
    	/// </summary>
        public int ErrorStringPosition { get; set; }
        
        public TokenParsingException(int stringPosition)
        {
            this.ErrorStringPosition = stringPosition;
        }
        
        public TokenParsingException()
        {
            ErrorStringPosition = -1;
        }

         public TokenParsingException(string message) : base(message)
        {
             ErrorStringPosition = -1;
        }

        public TokenParsingException(string message, Exception innerException) : base(message, innerException)
        {
            ErrorStringPosition = -1;
        }

        // This constructor is needed for serialization.
        protected TokenParsingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info != null) {
                this.ErrorStringPosition = info.GetInt32("ErrorStringPosition");
            }
        }
        
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("ErrorStringPosition", this.ErrorStringPosition);
        }
    }
}