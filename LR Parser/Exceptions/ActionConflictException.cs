﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 21.04.2017
 * Time: 15:10
 * 
 *
 */
using System;
using System.Runtime.Serialization;

namespace LR_Parser
{
    /// <summary>
    /// Ошибка при построении action таблицы LR1-автомата. Грамматика не принадлежит множеству LR1
    /// </summary>
    public class ActionConflictException : Exception, ISerializable
    {
    	/// <summary>
    	/// Первый пункт
    	/// </summary>
        public string FirstItem { get; set; }
        
        /// <summary>
        /// Второй пункт
        /// </summary>
        public string SecondItem { get; set; }
        
        /// <summary>
        /// Состояние автомата
        /// </summary>
        public int StateIndex { get; set; }
        
        public ActionConflictException(string firstItem, string secondItem, int stateIndex)
        {
            this.FirstItem = firstItem;
            this.SecondItem = secondItem;
            this.StateIndex = stateIndex;
        }
        
        public ActionConflictException()
        {
            init();
        }

        public ActionConflictException(string message) :  base(message)
        {
            init();
        }

        public ActionConflictException(string message, Exception innerException) : base(message, innerException)
        {
            init();
        }
        
        private void init()
        {
            this.FirstItem = "";
            this.SecondItem = "";
            this.StateIndex = -1;
        }

        // This constructor is needed for serialization.
        protected ActionConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info != null) {
                this.FirstItem = info.GetString("FirstItem");
                this.SecondItem = info.GetString("SecondItem");
                this.StateIndex = info.GetInt32("StateIndex");
            }
        }
        
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("FirstItem", this.FirstItem);
            info.AddValue("SecondItem", this.SecondItem);
            info.AddValue("StateIndex", this.StateIndex);
        }
    }
}