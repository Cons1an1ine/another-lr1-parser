﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 06.04.2017
 * Time: 15:53
 * 
 *
 */
using System;
using System.IO;
using System.Text;
using LR_Parser.Code;
using NUnit.Framework;

namespace TestsProject.TestingClasses
{
    /// <summary>
    /// Description of TokenizerTests.
    /// </summary>
    [TestFixture]
    public class TokenizerTests
    {
        public TokenizerTests()
        {
        }
        
        [Test]
        public void testSimpleParsing()
        {
            string str = @"Declaration ::= Type varName
            Type ::= integer
            Type ::= float";
            // Declaration Type varName integer float
            //      0        1     2       3      4
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var source = new Tokenizer(grammar.Symbols);
                
                source.setSource("integer  varName");
                Assert.AreEqual(3, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(2, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(Grammar.END_OF_STREAM, source.currentToken());
            }
        }
        
        [Test]
        public void testWithRegExpr()
        {
            string str = @"E ::= E + T
            E ::= E - T
            E ::= T
            T ::= T * F
            T ::= T / F
            T ::= F
            F ::= ( E )
            F ::= \d+\.?\d*";
            // E + T - * F / ( ) id
            // 0 1 2 3 4 5 6 7 8 9
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var source = new Tokenizer(grammar.Symbols);
                source.setSource("(33+1.0)/34");
                Assert.AreEqual(7, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(9, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(1, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(9, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(8, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(6, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(9, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(Grammar.END_OF_STREAM, source.currentToken());
            }
        }
        
        [Test]
        public void testWithRegExpr2()
        {
            string str = @"var ::= one two +
            one ::= 1+
            two ::= 2+";
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var source = new Tokenizer(grammar.Symbols);
                source.setSource("111222+");
                Assert.AreEqual(4, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(5, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(3, source.currentToken());
                source.findNextToken();
                Assert.AreEqual(Grammar.END_OF_STREAM, source.currentToken());
            }
        }
    }
}
