﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 05.04.2017
 * Time: 22:50
 * 
 *
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LR_Parser.Code;
using NUnit.Framework;

namespace TestsProject.TestingClasses
{
    /// <summary>
    /// Description of ParserTests.
    /// </summary>
    [TestFixture]
    public class ParserTests
    {
        public ParserTests()
        {
        }
        
        /// <summary>
        /// Простая леворекурсивная грамматика с одним терминалом в виде регулярного выражения
        /// </summary>
        [Test]
        public void testLR0LeftRecursiveGrammar()
        {
            
            string str = @"E ::= E + T
            E ::= E - T
            E ::= T
            T ::= T * F
            T ::= T / F
            T ::= F
            F ::= ( E )
            F ::= \d+\.?\d*";
            // \d+\.?\d*
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("1.+22/0.15*( 12 - 123 ) + (29)");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("1+(0.43/12*(33-16)+((22)))");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                // not match
                tokenizer.setSource("1+0.4-)");
                Assert.IsFalse(parser.match(tokenizer));
                
                tokenizer.setSource("12+((22)/15");
                Assert.IsFalse(parser.match(tokenizer));
            }
        }
        
        
        /// <summary>
        /// Простая праворекурсивная грамматика
        /// </summary>
        [Test]
        public void testLR0RightRecursiveGrammar()
        {
            
            string str = @"S ::= ( Ses )
            S ::= (* *)
            Ses ::= S SL
            SL ::= ; SL
            SL ::= S";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("(* *)");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( (* *) (* *) )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( (* *) ; (* *) )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("((**);;;;;;(**))");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                //not match
                tokenizer.setSource("( (* *) (* *)) )");
                Assert.IsFalse(parser.match(tokenizer));
                
                tokenizer.setSource("( (* *) ;*) (* *) )");
                Assert.IsFalse(parser.match(tokenizer));
            }
        }
        
        /// <summary>
        /// Простая грамматика с одним пустым порождением
        /// </summary>
        [Test]
        public void testLR0GrammarWithEmptyProduction()
        {
            
            string str = @"S ::= b A i B
            A ::=
            B ::= r C
            C ::= d";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("bird");
                Assert.AreEqual(true, parser.match(tokenizer));
            }
        }
        
        /// <summary>
        /// Тест грамматики с большим количесвом пустых порождений
        /// </summary>
        [Test]
        public void testSLRGrammarWithEmptyProduction()
        {
            
            string str = 
                @"S ::=    E
                E   ::= B + T
                E   ::= B * T
                E   ::= T
                
                B ::= C
                C ::= D
                D ::= E
                
                T ::= X
                X ::=";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("*+++");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("+*++");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("***");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("+ + + +");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("++ **");
                Assert.AreEqual(true, parser.match(tokenizer));
            }
        }

        [Test]
        public void testSLRGrammarWithEmptyProductionRightRecursive()
        {
            
            string str = 
                @"A ::=    a B
                A   ::= a A C
                A   ::= 
                
                B   ::= d B
                B   ::= e
                
                C   ::= c A";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("ac");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("ae");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("ade");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("acae");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("aaec");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("adde");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("acac");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("acade");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("aaddec");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("adddddde");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                // not match
                tokenizer.setSource("acce");
                Assert.IsFalse(parser.match(tokenizer));
                
                tokenizer.setSource("adec");
                Assert.IsFalse(parser.match(tokenizer));
            }
        }
        
        [Test]
        public void testLR1grammar()
        {
            string str = 
                @"E ::= id + D
                E ::= ( E * R )
                E ::=
                
                D ::= V * E
                D ::= L ! E
                
                R ::= V ! E
                R ::= L * E
                
                V ::= Z
                V ::= num
                
                L ::= Z
                L ::= ( E )
                
                Z ::=";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + !");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( * ! )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( * * )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + ( ) !");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + num *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( * ( ) * )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( * num ! )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + ! id + num *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + num * id + *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + * id + num *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + num * id + !");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + ( ) ! id + num *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + num * id + num *");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + num * id + ( ) !");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + ( id + num * ) !");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("id + num * ( * num ! )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( id + num * * num ! )");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("( * num ! id + num * )");
                Assert.AreEqual(true, parser.match(tokenizer));
            }
        }
        
        [Test]
        public void testLR1SimpleGrammar()
        {
            string str = 
                @"S ::= a g d
                S ::= a A c
                S ::= b A d
                S ::= b g c
                
                A ::= B
                
                B ::= g";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                var parser = new Parser(grammar);
                var tokenizer = new Tokenizer(grammar.Symbols);
                
                tokenizer.setSource("agc");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("bgc");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("bgd");
                Assert.AreEqual(true, parser.match(tokenizer));
                
                tokenizer.setSource("agd");
                Assert.AreEqual(true, parser.match(tokenizer));
            }
        }
        
        [Test]
        public void exceptionActionConflict()
        {
            string notLR1grammar = 
                @"E ::= E + E
                E ::= E * E
                E ::= ( E )
                E ::= id";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(notLR1grammar)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                try {
                    new Parser(grammar);
                } catch (LR_Parser.ActionConflictException ex) {
                    Assert.AreEqual(@"E ::= E + E . ,+", ex.FirstItem);
                    Assert.AreEqual(@"E ::= E . + E , \$", ex.SecondItem);
                    Assert.AreEqual(13, ex.StateIndex);
                }
            }
        }
    }
}
