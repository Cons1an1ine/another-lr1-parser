﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 22.12.2016
 * Time: 20:54
 * 
 *
 */
using System;
using NUnit.Framework;
using LR_Parser.Code;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace TestsProject.TestingClasses
{
    /// <summary>
    /// Description of GrammarReaderTests.
    /// </summary>
    [TestFixture]
    public class GrammarReaderTests
    {
        public GrammarReaderTests()
        {
        }

        private Grammar createOneRuleGrammar()
        {
            var symbols = new List<Symbol>();
            symbols.Add(new Symbol("Base", false));
            symbols.Add(new Symbol("variable"));
            symbols.Add(new Symbol("operator"));
            var rules = new List<Rule>();
            rules.Add(new Rule(0, new Production(new[] {1, 2, 1})));
            var sampleGrammar = new Grammar(symbols, rules);
            return sampleGrammar;
        }
        
        [Test]
        public void oneRuleParsing()
        {
            string str = "Base ::= variable operator variable";
            var sampleGrammar = createOneRuleGrammar();			
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                
                Assert.AreEqual(sampleGrammar, grammar);
            }
        }
        
        [Test]
        public void twoBaseSymbolsException()
        {
            string str = "one second ::= variable operator variable";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                Assert.Throws<LR_Parser.RuleFormatException>(() => grReader.parse(stream));
            }
        }
        
        [Test]
        public void incorrectBaseWritingException()
        {
            string str = " Incorrect.Base ::= variable operator variable";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                Assert.Throws<LR_Parser.RuleFormatException>(() => grReader.parse(stream));
            }
        }
        
        private Grammar createOneRuleEmptyProductionGrammar()
        {
            var symbols = new List<Symbol>();
            symbols.Add(new Symbol("Base", false));
            var rules = new List<Rule>(1);
            rules.Add(new Rule(0));
            var sampleGrammar = new Grammar(symbols, rules);
            return sampleGrammar;
        }
        
        [Test]
        public void emptyRuleParsing()
        {
            var sampleGrammar = createOneRuleEmptyProductionGrammar();
            string str = "	 Base ::=  	  ";
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                Assert.AreEqual(sampleGrammar, grReader.parse(stream));
            }
        }
        
        private Grammar createArithmeticGrammar()
        {
            /*
            E ::= E + T
            E ::= E - T
            E ::= T
            T ::= T * F
            T ::= T / F
            T ::= F
            F ::= ( E )
            F ::= \d+\.?\d*
            */
            var symbols = new List<Symbol>(); // E + T - * F / ( ) val
            symbols.Add(new Symbol("E", false));
            symbols.Add(new Symbol("+"));
            symbols.Add(new Symbol("T", false));
            symbols.Add(new Symbol("-"));
            symbols.Add(new Symbol("*"));
            symbols.Add(new Symbol("F", false));
            symbols.Add(new Symbol("/"));
            symbols.Add(new Symbol("("));
            symbols.Add(new Symbol(")"));
            symbols.Add(new Symbol(@"\d+\.?\d*"));
            
            /* Та же грамматика, записанная индексами
            0 ::= 0 1 2
              ::= 0 3 2
              ::= 2
            2 ::= 2 4 5
              ::= 2 6 5
              ::= 5
            5 ::= 7 0 8
              ::= 9
            */
            var rules = new List<Rule>();
            var productions = new List<Production>();
            productions.Add( new Production(new int[]{0 ,1 ,2}) );
            productions.Add( new Production(new int[]{0 ,3 ,2}) );
            productions.Add( new Production(new int[]{2}) );
            rules.Add(new Rule(0, productions));
            
            productions = new List<Production>();
            productions.Add( new Production(new int[]{2, 4, 5}) );
            productions.Add( new Production(new int[]{2, 6, 5}) );
            productions.Add( new Production(new int[]{5}) );
            rules.Add(new Rule(2, productions) );
            
            productions = new List<Production>();
            productions.Add( new Production(new int[]{7, 0, 8}) );
            productions.Add( new Production(new int[]{9}) );
            rules.Add(new Rule(5, productions) );
            
            return new Grammar(symbols, rules);
        }
        
        [Test]
        public void readAndParseArithmeticGrammar()
        {
            var sampleGrammar = createArithmeticGrammar();

            using(var stream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "\\Input Data\\ArithmeticGrammar.txt"))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                
                Assert.AreEqual(sampleGrammar, grammar);
            }
        }
    }
}
