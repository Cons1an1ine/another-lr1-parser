﻿/*
 * Created by SharpDevelop.
 * User: Kup
 * Date: 20.03.2017
 * Time: 21:15
 * 
 *
 */
using System;
using System.Text;
using LR_Parser.Code.Parts;
using NUnit.Framework;
using LR_Parser.Code;
using System.IO;
using System.Collections.Generic;

namespace TestsProject.TestingClasses
{
    /// <summary>
    /// Description of SetsGeneratorTesting.
    /// </summary>
    [TestFixture]
    public class FirstAndFollowTesting
    {
        public FirstAndFollowTesting()
        {
        }
        
        [Test]
        public void testFirstSetFinding()
        {
            string str = 
                @"Goal ::= A
                A      ::= Two
                A      ::= ( A )
                Two    ::= a
                Two    ::= b";
                        
            var firstSample = new List<int>[7];
            // 0-Goal   1-A   2-Two   3-(   4-)   5-a   6-b
            firstSample[0] = new List<int> {3, 5, 6}; // Goal
            firstSample[1] = new List<int> {3, 5, 6}; // A
            firstSample[2] = new List<int> {5, 6}; // Two
            firstSample[3] = new List<int> {3}; // (
            firstSample[4] = new List<int> {4}; // )
            firstSample[5] = new List<int> {5}; // a
            firstSample[6] = new List<int> {6}; // b
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                //List<int>[] firstSet = null;
                //bool[] nullable;

                //SetsGenerator.findFirstSet(grammar, out firstSet, out nullable);
                Assert.AreEqual(firstSample, grammar.First);
            }
        }
        
        [Test]
        public void testFollowSetFinding()
        {
            string str = 
                @"Goal ::= A
                A      ::= Two
                A      ::= ( A )
                Two    ::= a
                Two    ::= b";
                        
            var followSample = new List<int>[3];
            // 0-Goal   1-A   2-Two   3-(   4-)   5-a   6-b
            followSample[0] = new List<int> { Grammar.END_OF_STREAM }; // Goal
            followSample[1] = new List<int> {4, Grammar.END_OF_STREAM }; // A
            followSample[2] = new List<int> {4, Grammar.END_OF_STREAM }; // Two
            var endableSample = new bool[] {true, true, true};
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                
                //var followSet = SetsGenerator.findFollow(grammar);
                Assert.AreEqual(followSample, grammar.Follow);
                Assert.AreEqual(endableSample, grammar.Endable);
            }
        }
        
        [Test]
        public void testFirstSetWithEPSILON()
        {
            string str = 
                @"S ::=    for ( ExprOpt ; ExprOpt ; ExprOpt ) S
                S ::= expr ;
                
                ExprOpt ::=    expr
                ExprOpt ::=";
            
            // S for ( ExprOpt ; ) expr
            var firstSample = new List<int>[7];
            firstSample[0] = new List<int> { 1, 6}; // S
            firstSample[1] = new List<int> { 1 };
            firstSample[2] = new List<int> { 2 };
            firstSample[3] = new List<int> { 6 }; // ExprOpt
            firstSample[4] = new List<int> { 4 };
            firstSample[5] = new List<int> { 5 };
            firstSample[6] = new List<int> { 6 };

            bool[] emptySample = new bool[] { false, false, false, true, false, false, false };
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                //List<int>[] firstSet = null;
                //bool[] nullable;

                //SetsGenerator.findFirstSet(grammar, out firstSet, out nullable);
                Assert.AreEqual(firstSample, grammar.First);
                Assert.AreEqual(emptySample, grammar.Nullable);
            }
        }
        
        [Test]
        public void testFollowSetWithEPSILON()
        {
            string str = 
                @"S ::=    for ( ExprOpt ; ExprOpt ; ExprOpt ) S
                S ::= expr ;
                
                ExprOpt ::=    expr
                ExprOpt ::=";
            
            // S for ( ExprOpt ; ) expr
            // 0  1  2    3    4 5  6
            var followSample = new List<int>[2];
            followSample[0] = new List<int> { Grammar.END_OF_STREAM }; // S
            followSample[1] = new List<int> { 4, 5 }; // ExprOpt
            var endableSample = new bool[] {true, false};
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                
                //var followSet = SetsGenerator.findFollow(grammar);
                Assert.AreEqual(followSample, grammar.Follow);
                Assert.AreEqual(endableSample, grammar.Endable);
            }
        }
        
        [Test]
        public void testFirstSetWithEPSILON2()
        {
            string str = 
                @"S ::=    E
                E   ::= B + T
                E   ::= B * T
                E   ::= T
                
                B ::= C
                C ::= D
                D ::= E
                
                T ::= X
                X ::=";
            
            // S E B + T * C D X 
            // 0 1 2 3 4 5 6 7 8
            var firstSample = new List<int>[9];
            firstSample[0] = new List<int> { 3, 5 }; // S - {+ * epsilon}
            firstSample[1] = new List<int> { 3, 5 }; // E - {+ * epsilon}
            firstSample[2] = new List<int> { 3, 5 }; // B - {+ * epsilon}
            firstSample[3] = new List<int> { 3 }; // + - { + }
            firstSample[4] = new List<int> {  }; // T - { epsilon }  ?
            firstSample[5] = new List<int> { 5 }; // * - { * }
            firstSample[6] = new List<int> { 3, 5 }; // C - {+ * epsilon}
            firstSample[7] = new List<int> { 3, 5 }; // D - {+ * epsilon}
            firstSample[8] = new List<int> { }; // X - { epsilon } ?

            bool[] emptySample = new bool[] { true, true, true, false, true, false, true, true, true };
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                //List<int>[] firstSet = null;
                //bool[] nullable;

                //SetsGenerator.findFirstSet(grammar, out firstSet, out nullable);
                Assert.AreEqual(firstSample, grammar.First);
                Assert.AreEqual(emptySample, grammar.Nullable);
            }
        }
        
        [Test]
        public void testFollowSetWithEPSILON2()
        {
            string str = 
                @"S ::=    E
                E   ::= B + T
                E   ::= B * T
                E   ::= T
                
                B ::= C
                C ::= D
                D ::= E
                
                T ::= X
                X ::=";
            
            // S E B + T * C D X 
            // 0 1 2 3 4 5 6 7 8
            var followSample = new List<int>[7];
            followSample[0] = new List<int> { Grammar.END_OF_STREAM }; // S 
            followSample[1] = new List<int> { Grammar.END_OF_STREAM, 3, 5 }; // E 
            followSample[2] = new List<int> { 3, 5 }; // B 
            followSample[3] = new List<int> { 3, 5 }; // C
            followSample[4] = new List<int> { 3, 5 }; // D
            followSample[5] = new List<int> { Grammar.END_OF_STREAM, 3, 5 }; // T
            followSample[6] = new List<int> { Grammar.END_OF_STREAM, 3, 5}; // X
            var endableSample = new bool[] {true, true, false, false, false, true, true};
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                
                //var followSet = SetsGenerator.findFollow(grammar);
                Assert.AreEqual(followSample, grammar.Follow);
                Assert.AreEqual(endableSample, grammar.Endable);
            }
        }
        
        [Test]
        public void testFollowSetSLRGrammar()
        {
            string str = 
                @"EXP ::=    EXP add TERM
                EXP ::= TERM
                
                TERM ::= id
                TERM ::= id INDEX
                TERM ::= let STMTS in EXP end
                
                STMTS ::= STMTS STMT
                STMTS ::=
                
                STMT ::= LEXP assign EXP semi
                
                LEXP ::= LEXP INDEX
                LEXP ::= id
                
                INDEX ::= lpar EXP rpar";
            
            // EXP add TERM id INDEX let STMTS in end STMT LEXP assign semi lpar rpar
            //  0   1    2   3   4    5    6    7  8    9   10    11    12   13   14
            //  0        1       5         2            3    4 - nonterminals
            var followSample = new List<int>[6];
            followSample[0] = new List<int> { Grammar.END_OF_STREAM, 1, 8, 12, 14 };
            followSample[1] = new List<int> { Grammar.END_OF_STREAM, 1, 8, 12, 14 }; 
            followSample[2] = new List<int> { 7, 3 };
            followSample[3] = new List<int> { 7, 3 };
            followSample[4] = new List<int> { 11, 13 };
            followSample[5] = new List<int> { Grammar.END_OF_STREAM, 1, 8, 12, 14, 11, 13 };
            var endableSample = new bool[] {true, true, false, false, false, true};
            
            using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            using (var stream = new StreamReader(memoryStream))
            {
                var grReader = new GrammarReader();
                var grammar = grReader.parse(stream);
                
                //var followSet = SetsGenerator.findFollow(grammar);
                Assert.AreEqual(followSample, grammar.Follow);
                Assert.AreEqual(endableSample, grammar.Endable);
            }
        }
    }
}
